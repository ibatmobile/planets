import { Component, OnInit, Input } from '@angular/core';
import { PlanetsService } from '../../providers/planets.service';

@Component({
  selector: 'app-planets',
  templateUrl: 'planets.component.html',
})
export class PlanetsComponent implements OnInit {

  planets: Array<string>;
  @Input() title;
  constructor(private _planetService: PlanetsService) {

    this.planets = this._planetService.load();
  }

  ngOnInit() {
  }

}
