import { Component } from '@angular/core';

import { PlanetPage } from '../planet/planet';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = PlanetPage;


  constructor() {

  }
}
