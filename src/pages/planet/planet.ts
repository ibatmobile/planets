import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-planet',
  templateUrl: 'planet.html'
})
export class PlanetPage {

  constructor(public navCtrl: NavController) {

  }

}
