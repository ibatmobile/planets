import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { PlanetPage } from '../pages/planet/planet';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//@TODO - Add these two lines
import { PlanetsComponent } from '../components/planets/planets.component';
import { PlanetsService } from '../providers/planets.service';



@NgModule({
  declarations: [
    MyApp,
    PlanetPage,

    HomePage,
    TabsPage,
     PlanetsComponent // @TODO
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PlanetPage,
  
    HomePage,
    TabsPage,
     PlanetsComponent //@TODO
  ],
  providers: [
    StatusBar,
    SplashScreen,
      PlanetsService, //@TODO
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
