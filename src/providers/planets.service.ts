import { Injectable } from '@angular/core';

@Injectable()
export class PlanetsService {

  constructor() { }

  load(): Array<string> {

    return ["Mercury", "Venus", "Earth", "Mars", "Saturn", "Jupiter"];
  }
}
